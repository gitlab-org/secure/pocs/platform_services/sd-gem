#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

setup_suite() {
  docker run -d -p "8080":"8080" --name sd-gem-poc-test sd-gem-poc
}

teardown_suite() {
  docker rm -f sd-gem-poc-test > /dev/null 2>&1
}

test_e2e() {
  sleep 15
  curl -XPOST 'localhost:8080/scan' --data 'glpat-12312312312312312312'

  assert_equals "0" "$?" "Expected to exit without errors"
}

