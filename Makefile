gem:
	git clone --depth 1 --no-checkout git@gitlab.com:gitlab-org/gitlab.git
	/bin/bash -c '(cd gitlab && git sparse-checkout init --cone)'
	/bin/bash -c '(cd gitlab && git sparse-checkout set gems/gitlab-secret_detection)'
	/bin/bash -c '(cd gitlab && git checkout)'
	cp -r gitlab/gems/gitlab-secret_detection gems/gitlab-secret_detection
	rm -rf gitlab

image:
	docker build -t sd-gem-poc .

test: image
	bash_unit ./test/e2e.sh

deploy:
	gcloud run deploy
