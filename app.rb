require 'securerandom'
require 'sinatra'

require 'gitlab/secret_detection'


set :bind, "0.0.0.0"
port = ENV["PORT"] || "8080"
set :port, port

get "/" do
  "Gitlab::SecretDetection v#{Gitlab::SecretDetection::VERSION}!"
end

post "/scan" do
  id = SecureRandom.base64(10)
  data = request.body.read
  blobs = [
    Struct.new(:id, :data).new(id, data)
  ]
  scan = Gitlab::SecretDetection::Scan.new
  resp = scan.secrets_scan(
    blobs
  )

  p '*'*80
  p resp
  p '*'*80

  if resp.results.any?
    status 200
    resp.results.map { |r| "#{r.type} #{r.line_number}" }.join("\n")
  else
    status 204
  end
end
